//
//  LoginViewController.swift
//  empresas-ios
//
//  Created by user128675 on 17/08/17.
//  Copyright © 2017 iOasys. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {

    @IBOutlet weak var textFieldEmail: UITextField!
    
    @IBOutlet weak var textFieldSenha: UITextField!
    
    @IBOutlet weak var buttonEntrar: UIButton!
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        putBorderBottom(textField: textFieldEmail)
        putImageSideInTextField(textField: textFieldEmail, imageName: "icEmail")
        putBorderBottom(textField: textFieldSenha)
        putImageSideInTextField(textField: textFieldSenha, imageName: "icCadeado")
        buttonEntrar.layer.masksToBounds = true
        buttonEntrar.layer.cornerRadius = 6
        
        //NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        //NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
    }
    
    func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            if self.view.frame.origin.y == 0{
                self.view.frame.origin.y -= keyboardSize.height
            }
        }
    }
    
    func keyboardWillHide(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            if self.view.frame.origin.y != 0{
                self.view.frame.origin.y += keyboardSize.height
            }
        }
    }
    
    func putBorderBottom(textField: UITextField){
        let border = CALayer()
        let width = CGFloat(1.0)
        let borderColor = UIColor(red: 56/255, green: 55/255, blue: 67/255, alpha: 1)
        border.borderColor = borderColor.cgColor
        border.frame = CGRect(x: 0, y: textField.frame.size.height - width, width:  textField.frame.size.width, height: textField.frame.size.height)
        border.borderWidth = width
        textField.layer.addSublayer(border)
        textField.layer.masksToBounds = true
    }

    func putImageSideInTextField(textField: UITextField, imageName: String){
        let image = UIImage(named: imageName);
        textField.leftViewMode = UITextFieldViewMode.always
        textField.leftView = UIImageView(image: image)
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
